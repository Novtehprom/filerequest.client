﻿using FileRequest.Client.Service;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;

namespace FileRequest.Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var manager = new RequestManager();
            //var result = manager.GetFile("https://localhost:44363/ ", "http://bass32.ru/MainPage/GetImage/6");
            var result = manager.GetFile("http://testfileload-ru.1gb.ru/", "http://bass32.ru/MainPage/GetImage/6");
            if ( result.Result is byte[] a)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "CSV Files(*.jpg)|*.png|All(*.*)|*";
                dialog.RestoreDirectory = true;
                dialog.InitialDirectory = dialog.FileName;

                try
                {
                    if (dialog.ShowDialog() == true)
                    {
                        string path = dialog.FileName;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            stream.Write(a, 0, a.Length);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error! " + ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }
}
