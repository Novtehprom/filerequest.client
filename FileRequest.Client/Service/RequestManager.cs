﻿using FileRequest.Core;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FileRequest.Client.Service
{
    public class RequestManager
    {
        public RequestState GetFile(string baseUrl, string fileUrl)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.PostAsync("weatherforecast?url="+ fileUrl, null);
                if (!response.Result.IsSuccessStatusCode) return null;
                RequestState result = JsonConvert.DeserializeObject<RequestState>(response.Result.Content.ReadAsStringAsync().Result);
                result.Result = JsonConvert.DeserializeObject<byte[]>(result.Result.ToString());
                return result;
            }
        }
    }
}
